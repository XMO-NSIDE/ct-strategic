# Qlik Application Repository 
# strategic-2021_2-RC2
### 
Created By XMO-NSIDE(Xavier Morosini) at Tue Jun 01 2021 11:25:07 GMT+0200 (Central European Summer Time)

Branch Name|Qlik application
---|---
master|[https://qlik-dev-feb19.n-side.com/sense/app/0704ce19-ecb1-48e6-84af-8436b80de38e](https://qlik-dev-feb19.n-side.com/sense/app/0704ce19-ecb1-48e6-84af-8436b80de38e)
develop|[https://qlik-dev-feb19.n-side.com/sense/app/83973c32-94e0-44a2-9106-a29cd6d85c4e](https://qlik-dev-feb19.n-side.com/sense/app/83973c32-94e0-44a2-9106-a29cd6d85c4e)
strategic_4_2|[https://qlik-dev-feb19.n-side.com/sense/app/d59ad991-56ca-47f8-9646-a126ecad3268](https://qlik-dev-feb19.n-side.com/sense/app/d59ad991-56ca-47f8-9646-a126ecad3268)
stategic-improvement|[https://qlik-dev-feb19.n-side.com/sense/app/6be15f61-8743-4817-988c-adfd93a21f30](https://qlik-dev-feb19.n-side.com/sense/app/6be15f61-8743-4817-988c-adfd93a21f30)
release_5_0|[https://qlik-dev-nov21.n-side.com/sense/app/7ce2e7c1-c222-435c-99a4-0fd41ff146b8](https://qlik-dev-nov21.n-side.com/sense/app/7ce2e7c1-c222-435c-99a4-0fd41ff146b8)

Branch Name|Qlik application
---|---
strategic_6_0|[https://qlik-dev-nov21.n-side.com/sense/app/dc22379b-e863-4f99-a537-40996c70456c](https://qlik-dev-nov21.n-side.com/sense/app/dc22379b-e863-4f99-a537-40996c70456c)

Branch Name|Qlik application
---|---
maintenance|[https://qlik-dev-nov21.n-side.com/sense/app/95ec56d0-9924-4b51-b7fb-ccbc9715e7cf](https://qlik-dev-nov21.n-side.com/sense/app/95ec56d0-9924-4b51-b7fb-ccbc9715e7cf)

Branch Name|Qlik application
---|---
strategic_6_0|[https://qlik-dev-nov21.n-side.com/sense/app/795513fb-29d3-457d-8c7f-5bd728eebb09](https://qlik-dev-nov21.n-side.com/sense/app/795513fb-29d3-457d-8c7f-5bd728eebb09)

Branch Name|Qlik application
---|---
maintenance|[https://qlik-dev-nov21.n-side.com/sense/app/a14a522f-38a0-4455-8cb3-ca1331d9fa51](https://qlik-dev-nov21.n-side.com/sense/app/a14a522f-38a0-4455-8cb3-ca1331d9fa51)